"""
Animation management
"""
import copy

import matplotlib.patches
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

from simple_2d_engine.world import World


def play_animation(world: World, n_frames=1000, time_interval=0.01, speed=1, x_lim=(0, 1), y_lim=(0, 1),
                   trace=False, trace_color=None, trace_line_style=None,
                   save_to_js_html=False, animation_file='./movie.html'):
    """
    Animate a world.
    :param world: World to animate, must contains game objects
    :param n_frames: number of frames of the animation
    :param time_interval: (s) interval between two frames
    :param speed: physics speed relative to animation speed; set to < 1 for slow motion, > 1 for fast motion
    :param x_lim: (m) animation boundaries on the x-axis
    :param y_lim: (m) animation boundaries on the y-axis
    :param trace: if True, display the trajectory of the center of moving objects
    :param trace_color: color of the trace
    :param trace_line_style: line style of the trace
    :param save_to_js_html: if True, save the animation as a HTML file
    :param animation_file: name of the HTML file
    :return: the animation object, the x and y trace data of each object, the time array and the physics delta time
    """
    # Initialize variables
    fig, ax = plt.subplots()

    animation_interval = time_interval * 1000  # s to ms
    time_interval *= speed

    x_trace = []
    y_trace = []

    other_lines = {}

    for _ in range(np.size(world.objects)):
        x_trace.append([])
        y_trace.append([])

    # Initialize trace style: if None, take the style of the sprite of each objects
    if np.size(trace_color) == 1:
        if trace_color is None:
            trace_color = []

            for o in world.objects:
                trace_color.append(o.sprite.get_edgecolor())
        elif not hasattr(trace_color, '__iter__'):
            color_tmp = trace_color
            trace_color = []

            for i in range(np.size(world.objects)):
                trace_color.append(color_tmp)

    if np.size(trace_line_style) == 1:
        if trace_line_style is None:
            trace_line_style = []

            for o in world.objects:
                trace_line_style.append(o.sprite.get_linestyle())
        elif not hasattr(trace_color, '__iter__'):
            ls_tmp = trace_line_style
            trace_line_style = []

            for i in range(np.size(world.objects)):
                trace_line_style.append(ls_tmp)

    # Animation functions definitions
    def init():
        """Initialize the animation."""
        ax.clear()  # ensure that the figure is clean
        patches = []

        # Add the sprite of all objects and initialize traces
        for j, obj in enumerate(world.objects):
            # Ensure that the world objects are in their initial state
            obj.__dict__.update(copy.deepcopy(world_objects_init_state[j].__dict__))

            # Draw the objects
            patches.append(ax.add_patch(copy.copy(obj.sprite)))

            line = ax.plot([], [], color=trace_color[j], linestyle=trace_line_style[j])[0]
            line.set_data(obj.rigid_body.position.x, obj.rigid_body.position.y)
            patches.append(line)

        ax.set_xlim(x_lim[0], x_lim[1])
        ax.set_ylim(y_lim[0], y_lim[1])
        ax.set_aspect(1)

        return patches

    def update(frame, *args):
        """Update the animation."""
        delta_time = args[1]  # time between two frames
        patches = []

        for patch in ax.patches:
            patches.append(patch)

        for j, obj in enumerate(args[0]):
            # Reset traces at the first frame
            if frame == 0:
                args[2][j] = []
                args[3][j] = []
                args[4][j] = None

            # Move all objects
            args[0][j].move(world, delta_time)

            if isinstance(args[0][j].sprite, matplotlib.patches.Circle):
                patches[j].center = args[0][j].sprite.center
            else:
                patches[j].xy = args[0][j].sprite.xy

            # Append the traces of the objects
            if trace:
                args[2][j].append(args[0][j].rigid_body.position.x)
                args[3][j].append(args[0][j].rigid_body.position.y)

            ax.lines[j].set_data(args[2][j], args[3][j])
            patches.append(ax.lines[j])

            if obj.sprite.get_edgecolor() != trace_color[j]:
                if j not in args[4]:
                    # Save previous trace color as a new line
                    ax.lines[j].set_color(trace_color[j])
                    args[4][j] = ax.lines[j]

                patches.append(args[4][j])

        # Print a message at the end of the animation
        if frame == n_frames - 1:
            print('Animation ended, close the figure window to continue.')

        return patches

    # Save the initial state of animated objects if the animation needs to restart (e.g. when saving)
    world_objects_init_state = copy.deepcopy(world.objects)

    # Play animation
    animation = FuncAnimation(
        fig, update, frames=n_frames, interval=animation_interval, init_func=init, blit=True,
        fargs=(world.objects, time_interval, x_trace, y_trace, other_lines), repeat=False)

    # Outputs
    if save_to_js_html:
        with open(f'{animation_file}', 'w') as f:
            f.write(animation.to_jshtml())

    time = np.arange(0, n_frames * time_interval, time_interval)

    return animation, x_trace, y_trace, time, time_interval, fig, ax
