"""
Colliders objects.
"""
import sys
import warnings

import numpy as np


class Collider:
    """
    Represents the physical shape of rigid bodies and manage collisions.
    """

    def __init__(self):
        self.area = self.get_area()
        self.cross_section = self.get_cross_section()
        self.volume = self.get_volume()
        self.is_colliding = False
        self.other_collider = None

    def get_area(self):
        return 0

    def get_cross_section(self):
        return 0

    def get_volume(self):
        return 0

    def bounce(self, self_rigid_body, other_rigid_body, delta_time):
        return self_rigid_body.velocity


class Box(Collider):
    """
    Collider defined by four vertices. The collider do not rotate and its edges stay parallel to the world axes.
    """

    def __init__(self, size=None, rotation=None):
        if size is None:
            size = Vector3D(0, 0, 0)
        elif np.size(size) == 2:
            size = Vector3D(size[0], size[1], 1.0)
        elif np.size(size) == 3:
            size = Vector3D(size[0], size[1], size[2])

        if rotation is None:
            rotation = Vector3D(0, 0, 0)
        elif np.size(rotation) == 0:
            rotation = Vector3D(0, 0, rotation)
        elif np.size(rotation) == 1:
            if not hasattr(rotation, '__iter__'):
                rotation = [rotation]

            rotation = Vector3D(0, 0, rotation[0])
        elif np.size(rotation) == 3:
            rotation = Vector3D(rotation[0], rotation[1], rotation[2])  # not used

        self.size = size
        self.rotation = rotation

        super().__init__()

    def get_area(self):
        return 2 * (self.size.x * self.size.y + self.size.x * self.size.z + self.size.y * self.size.z)

    def get_cross_section(self):
        return self.size.x * self.size.z  # TODO cross section depending on rotation

    def get_volume(self):
        return self.size.x * self.size.y * self.size.z

    def _box_box_collision(self, self_rigid_body, box, delta_time):
        """
        Collision between two non-rotated boxes.
        :param self_rigid_body: parent rigid body of this collider
        :param box: another rigid body with a Box collider
        :param delta_time: (s) physics delta time
        :return: 2D-array (the normal of the collision), and True if the collision occurred (False otherwise)
        """
        self_pos = self_rigid_body.update_position(delta_time)
        self_current_pos = self_rigid_body.position
        other_pos = box.update_position(delta_time)
        new_velocity = self_rigid_body.velocity

        if other_pos.x - self.size.x < self_current_pos.x < other_pos.x + box.collider.size.x:
            if other_pos.y - self.size.y < self_pos.y < other_pos.y + box.collider.size.y:
                new_velocity.y = -new_velocity.y * self_rigid_body.bounce_coefficient

                return new_velocity, True
            else:
                return new_velocity, False
        elif other_pos.y - self.size.y < self_current_pos.y < other_pos.y + box.collider.size.y:
            if other_pos.x - self.size.x < self_pos.x < other_pos.x + box.collider.size.x:
                new_velocity.x = -new_velocity.x * self_rigid_body.bounce_coefficient

                return new_velocity, True
            else:
                return new_velocity, False
        else:
            return new_velocity, False

    def bounce(self, self_rigid_body, other_rigid_body, delta_time):
        """
        Detect if a collision is occurring with another rigid body and modify the velocity accordingly.
        :param self_rigid_body: parent rigid body of this collider
        :param other_rigid_body: another rigid body
        :param delta_time: (s) physics delta time
        :return: 2DVector representing the velocity of the rigid body after the collision
        """
        if isinstance(other_rigid_body.collider, Box):
            new_velocity, is_colliding = \
                self._box_box_collision(self_rigid_body, other_rigid_body, delta_time)
            self.other_collider = other_rigid_body.collider
        else:
            new_velocity = self_rigid_body.velocity
            warnings.warn(f"collision between box collider and {type(other_rigid_body.collider)} not implemented")
            self.other_collider = None

        return new_velocity


class Capsule(Collider):
    """
    A capsule is made of a cylinder and two hemispheres at the top and bottom of the cylinder.
    The radius define the radius of the cylinder and of the hemispheres. The height refer to the total height of the
    shape, i.e. the height of the cylinder plus the radius of each hemisphere.
    """

    def __init__(self, radius=0.0, height=0.0):
        self.radius = radius
        self.height = height

        super().__init__()

    def get_area(self):
        cylinder_area = Cylinder(self.radius, self.height - self.radius).area
        sphere_area = Sphere(self.radius).area
        return cylinder_area + sphere_area

    def get_cross_section(self):
        """
        Get the cross-section along the height axis of a capsule.
        """
        return Sphere(self.radius).cross_section

    def get_volume(self):
        cylinder_volume = Cylinder(self.radius, self.height - self.radius).volume
        sphere_volume = Sphere(self.radius).volume
        return cylinder_volume + sphere_volume


class Cylinder(Collider):
    """
    Cylindrical Collider.
    """

    def __init__(self, radius=0.0, height=0.0):
        self.height = height
        self.radius = radius

        super().__init__()

    def get_area(self):
        return 2 * np.pi * (self.radius * self.height + self.radius ** 2)

    def get_cross_section(self):
        """
        Get the cross-section along the radius axis of a cylinder.
        """
        return 2 * self.radius * self.height

    def get_volume(self):
        return np.pi * self.radius ** 2 * self.height


class Edge(Collider):
    """
    Simple edge collider defined by two vertices.
    """

    def __init__(self, length=None, rotation=None):
        if length is None:
            length = 0

        if rotation is None:
            rotation = Vector3D(0, 0, 0)
        elif np.size(rotation) == 1:
            if not hasattr(rotation, '__iter__'):
                rotation = [rotation]

            rotation = Vector3D(0, 0, rotation[0])
        elif np.size(rotation) == 3:
            rotation = Vector3D(rotation[0], rotation[1], rotation[2])  # not used

        self.length = length
        self.rotation = rotation

        super().__init__()

    def get_vertices(self, self_rigid_body):
        """
        Get the coordinates of the vertices.
        :param self_rigid_body: parent rigid body of teh collider
        :return:
        """
        vertex_1 = Vector2D(self_rigid_body.position.x, self_rigid_body.position.y)
        vertex_2 = Vector2D(
            self_rigid_body.position.x + self.length * np.cos(np.deg2rad(self.rotation.z)),
            self_rigid_body.position.y + self.length * np.sin(np.deg2rad(self.rotation.z))
        )

        return vertex_1, vertex_2

    def update_vertices_position(self, self_rigid_body, delta_time):
        next_position = self_rigid_body.update_position(delta_time)
        vertex_1 = Vector2D(next_position.x, next_position.y)
        vertex_2 = Vector2D(
            next_position.x + self.length * np.cos(np.deg2rad(self.rotation.z)),
            next_position.y + self.length * np.sin(np.deg2rad(self.rotation.z))
        )

        return vertex_1, vertex_2

    def get_normal(self, self_rigid_body, delta_time, at_next_step=False):
        if not at_next_step:
            vertex_1, vertex_2 = self.get_vertices(self_rigid_body)
        else:
            vertex_1, vertex_2 = self.update_vertices_position(self_rigid_body, delta_time)

        dx = vertex_1.x - vertex_2.x
        dy = vertex_1.y - vertex_2.y

        normal = np.array([dy, -dx])
        normal /= np.linalg.norm(normal)

        return normal, vertex_1, vertex_2


class Sphere(Collider):
    """
    Spherical collider.
    """

    def __init__(self, radius=0.0):
        self.radius = radius

        super().__init__()

    def get_area(self):
        return 4 * np.pi * self.radius ** 2

    def get_cross_section(self):
        return np.pi * self.radius ** 2

    def get_volume(self):
        return 4 / 3 * np.pi * self.radius ** 3

    def _sphere_sphere_collision(self, self_rigid_body, sphere, delta_time):
        """
        Collision between two spheres.
        :param self_rigid_body: parent rigid body of this rigid body
        :param sphere: another rigid body with a Sphere collider
        :param delta_time: (s) physics delta time
        :return: 2D-array of the normal of the collision, or None if no collision is occurring with the other body
        """
        # TODO implement elastic collision https://en.wikipedia.org/wiki/Elastic_collision
        next_self_position = self_rigid_body.update_position(delta_time)
        next_other_position = sphere.update_position(delta_time)

        dx = next_self_position.x - next_other_position.x
        dy = next_self_position.y - next_other_position.y

        distance = np.max((np.sqrt(dx ** 2 + dy ** 2), sys.float_info.min))  # avoid division by 0

        normal = np.array([dx, dy]) / distance  # the normal is the vector between the two spheres

        if distance < self.radius + sphere.collider.radius:
            return normal, True
        else:
            return normal, False

    def _sphere_edge_collision(self, self_rigid_body, edge, delta_time):
        """
        Collision between a sphere and an edge.
        The sphere collide with the edge only if it touches any point between its two vertices, hence the colliding
        hit box of the edge has the shape of a capsule with a radius equal to the radius of the sphere.
        :param self_rigid_body: parent rigid body of this rigid body
        :param edge: another rigid body with a Sphere collider
        :param delta_time: (s) physics delta time
        :return: 2D-array (the normal of the collision), and True if the collision occurred (False otherwise)
        """
        # Get the next positions of the sphere and the edge
        next_position = self_rigid_body.update_position(delta_time)

        # Get the normal of the edge
        normal, edge_vertex_1, edge_vertex_2 = edge.collider.get_normal(edge, delta_time, at_next_step=True)

        # Get the distance between the center of the sphere and the first vertex of the edge
        edge_sphere_vector_1 = np.array([
            next_position.x - edge_vertex_1.x,
            next_position.y - edge_vertex_1.y
        ])
        distance = np.dot(edge_sphere_vector_1, normal)  # projection of the vector on the normal

        # Handle fast objects
        if np.linalg.norm(self_rigid_body.velocity.to_array()) * delta_time > 2 * self.radius \
                and np.abs(distance) < np.linalg.norm(self_rigid_body.velocity.to_array()) * delta_time:
            edge_sphere_vector_current_step = np.array([
                self_rigid_body.position.x - edge_vertex_1.x,
                self_rigid_body.position.y - edge_vertex_1.y
            ])
            distance_current_step = np.dot(edge_sphere_vector_current_step, normal)

            # If the sign of the distance changes, it means that the sphere is going through the edge
            if np.sign(distance_current_step) != np.sign(distance):
                distance = self.radius * (1 - 1e-12)

        # Collision is possible
        if np.abs(distance) < self.radius:
            # Get the maximum of the distances between the center of the sphere and the two edge's vertices
            edge_sphere_vector_2 = np.array([
                next_position.x - edge_vertex_2.x,
                next_position.y - edge_vertex_2.y
            ])

            norm_1 = np.linalg.norm(edge_sphere_vector_1)
            norm_2 = np.linalg.norm(edge_sphere_vector_2)

            max_distance = np.max([norm_1, norm_2])

            # Calculate the maximum possible colliding distance between the furthest vertex of the edge from the sphere
            # and the projection of the center of the sphere on the edge
            max_colliding_distance = edge.collider.length + np.sqrt(self.radius ** 2 - distance ** 2)

            # Calculate the maximum possible colliding distance between the furthest vertex of the edge from the sphere
            # and the center of the sphere
            max_colliding_distance = np.sqrt(max_colliding_distance ** 2 + distance ** 2)

            if max_distance < max_colliding_distance:
                return normal, True
            else:
                return normal, False
        else:
            return normal, False

    def _sphere_box_collision(self, self_rigid_body, box, delta_time):
        """
        Collision between a sphere and a non-rotated box.
        :param self_rigid_body: parent rigid body of this collider
        :param box: another rigid body with a Box collider
        :param delta_time: (s) physics delta time
        :return: 2D-array (the normal of the collision), and True if the collision occurred (False otherwise)
        """
        self_pos = self_rigid_body.update_position(delta_time)
        self_current_pos = self_rigid_body.position
        other_pos = box.update_position(delta_time)
        new_velocity = self_rigid_body.velocity

        if other_pos.x - self.radius < self_current_pos.x < other_pos.x + box.collider.size.x + self.radius:
            if other_pos.y - self.radius < self_pos.y < other_pos.y + box.collider.size.y + self.radius:
                new_velocity.y = -new_velocity.y

                return new_velocity, True
            else:
                return new_velocity, False
        elif other_pos.y - self.radius < self_current_pos.y < other_pos.y + box.collider.size.y + self.radius:
            if other_pos.x - self.radius < self_pos.x < other_pos.x + box.collider.size.x + self.radius:
                new_velocity.x = -new_velocity.x

                return new_velocity, True
            else:
                return new_velocity, False
        else:
            return new_velocity, False

    @staticmethod
    def collide(self_rigid_body, collision_normal):
        """
        Change in velocity after an object colliding with an immovable surface.
        Source: https://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector
        Given the velocity vector v and the normal n:
            - v_n the projection of v on n is norm(v) * cos(a), a is the angle between v and n
            - this is equivalent to v_n = norm(v) * norm(n) * cos(a) = dot(v, n) since norm(n) = 1
            - to get the direction of v_n, we multiply by n -> v_n_ = dot(v, n) * n
            - v can be written as the sum of two perpendicular vectors: v = w + v_n_
            - w = v - v_n_
            - r the reflected velocity vector has the same w (perpendicular) than v, but inverted v_n_ (parallel)
            - r = w - v_n_ = v - v_n_ - v_n_ = v - 2 * dot(v, n) * n
        :param self_rigid_body: parent rigid body of the collider
        :param collision_normal: normal of the surface, must be normalized
        :return:
        """
        velocity = np.array([self_rigid_body.velocity.x, self_rigid_body.velocity.y])
        new_velocity = velocity - 2 * np.dot(velocity, collision_normal) * collision_normal

        # Apply bounce coefficient
        velocity_normal = np.dot(new_velocity, collision_normal) * collision_normal
        new_velocity = new_velocity - velocity_normal * (1 - self_rigid_body.bounce_coefficient)

        return new_velocity

    @staticmethod
    def roll(self_rigid_body, delta_time, collision_normal):
        """
        Rules to follow if a body is not bouncing fast enough from another body.
        :param self_rigid_body:
        :param delta_time:
        :param collision_normal:
        :return:
        """
        # Get the sum of the forces currently applied on the body
        sum_forces = Vector2D.to_array(self_rigid_body.sum_forces(delta_time))

        # Calculate the reaction force
        reaction = np.dot(collision_normal, sum_forces) * -collision_normal

        # Translate the reaction force into acceleration and velocity
        acceleration = reaction / self_rigid_body.mass
        new_velocity = Vector2D.to_array(self_rigid_body.velocity) + acceleration * delta_time

        # Project the velocity of the body along a direction perpendicular to the normal
        direction_vector = np.array([-collision_normal[1], collision_normal[0]])
        new_velocity = np.dot(new_velocity, direction_vector) * direction_vector

        new_velocity = Vector2D.from_array(new_velocity)

        return new_velocity

    def bounce(self, self_rigid_body, other_rigid_body, delta_time):
        """
        Detect if a collision is occurring with another rigid body and modify the velocity accordingly.
        :param self_rigid_body: parent rigid body of this collider
        :param other_rigid_body: another rigid body
        :param delta_time: (s) physics delta time
        :return: 2DVector representing the velocity of the rigid body after the collision
        """
        new_velocity = None

        if isinstance(other_rigid_body.collider, Sphere):
            collision_normal, is_colliding = \
                self._sphere_sphere_collision(self_rigid_body, other_rigid_body, delta_time)
        elif isinstance(other_rigid_body.collider, Edge):
            collision_normal, is_colliding = \
                self._sphere_edge_collision(self_rigid_body, other_rigid_body, delta_time)
        elif isinstance(other_rigid_body.collider, Box):  # special case
            new_velocity, is_colliding = \
                self._sphere_box_collision(self_rigid_body, other_rigid_body, delta_time)
            collision_normal = None
            self.other_collider = other_rigid_body.collider
        else:
            self.other_collider = None
            collision_normal = None
            is_colliding = False

        if is_colliding and collision_normal is not None:
            self.other_collider = other_rigid_body.collider

            if not self.is_colliding:
                self.is_colliding = True
                new_velocity = self.collide(self_rigid_body, collision_normal)
                new_velocity = Vector2D.from_array(new_velocity)
            else:
                self.is_colliding = True
                new_velocity = self.roll(self_rigid_body, delta_time, collision_normal)
        elif not isinstance(other_rigid_body.collider, Box):
            self.is_colliding = False
            self.other_collider = None
            new_velocity = self_rigid_body.velocity

        return new_velocity


class Vector2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def to_array(self):
        return np.array([self.x, self.y])

    @classmethod
    def from_array(cls, array):
        return cls(
            x=array[0],
            y=array[1],
        )


class Vector3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def to_array(self):
        return np.array([self.x, self.y, self.z])

    @classmethod
    def from_array(cls, array):
        return cls(
            x=array[0],
            y=array[1],
            z=array[2],
        )
