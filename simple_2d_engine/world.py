"""
World object.
"""
import numpy as np


class World:
    infinity = np.finfo(float).max  # approximate infinity with a very large number

    def __init__(self, objects=None, gravity=9.8, fluid_density=1.2, boundaries: np.ndarray = None,
                 bounce_enabled=True):
        """
        The world in which the objects are.
        :param objects: GameObjects in the world
        :param gravity: (m.s-2) gravity of the world, always vertical, positive toward the floor/bottom
        :param fluid_density: (kg.m-3) fluid density of the world
        :param boundaries: 2 points array defined as [[x_left_wall, y_ground], [x_right_wall, y_ceiling]]
        :param bounce_enabled: enable rigid bodies to bounce on boundaries
        """
        if objects is None:
            objects = np.array([])

        self.objects = objects
        self.gravity = gravity
        self.fluid_density = fluid_density

        if boundaries is None:
            boundaries = np.array([[-World.infinity, -World.infinity], [World.infinity, World.infinity]])

        self.boundaries = boundaries
        self.bounce_enabled = bounce_enabled
