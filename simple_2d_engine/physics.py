"""
Physics functions and objects.
"""
import numpy as np


from simple_2d_engine.colliders import Box, Sphere, Vector2D
from simple_2d_engine.world import World


class Force(Vector2D):
    def __init__(self, vector_2d: Vector2D):
        super().__init__(vector_2d.x, vector_2d.y)

    def update(self, rigid_body, delta_time=None):
        """
        Update the force based on the rigid body parameters.
        :param rigid_body: a rigid body
        :param delta_time: (s) physics delta time
        """
        pass


class Drag(Force):
    """
    Drag force at high (>~ 1000) Reynolds number.
    """

    def __init__(self, fluid_density):
        super().__init__(Vector2D(0, 0))

        self.fluid_density = fluid_density

    def _get_drag(self, rigid_body):
        """
        Calculate the drag force experienced by a rigid body.
        Greatly overestimate the drag force at high velocities/high delta time.
        Since the drag force depends on the velocity, it can causes weird behaviour (rigid body "bouncing" on air) or
        even instabilities (when drag / mass * delta_time > 2 * velocity) at high velocities/high delta time.
        This happens because the drag force at the beginning of the time step (at high velocity) is applied during all
        the time step, while in reality it decreases (because the drag force decreases the velocity).
        :param rigid_body: a rigid body
        :return: (N) the drag force
        """
        velocity = rigid_body.velocity.to_array()

        drag_norm = np.linalg.norm(
            0.5 * self.fluid_density * velocity ** 2
            * rigid_body.drag_coefficient * rigid_body.collider.cross_section
        )
        drag_angle = np.arctan2(rigid_body.velocity.y, rigid_body.velocity.x) + np.pi  # angle is opposite of velocity

        drag = np.array([
            drag_norm * np.cos(drag_angle),
            drag_norm * np.sin(drag_angle)
        ])

        return drag

    def _get_drag_accurate(self, rigid_body, delta_time):
        """
        Calculate the drag force experienced by a rigid body using the integral formulae.

        It is advantageous to use this formalism instead of calculating directly the drag because of the following.
        Drag force directly calculated can be greatly overestimated at high velocities/high delta time.
        Since the drag force depends on the velocity, it can causes weird behaviour (rigid body "bouncing" on air) or
        even instabilities (when drag / mass * delta_time > 2 * velocity) at high velocities/high delta time.
        This happens because the drag force at the beginning of the time step (at high velocity) is applied during all
        the time step, while in reality it decreases (because the drag force decreases the velocity).

        Instead, by considering that the current time step is at t = 0 and that the next time step is at t = Dt, the
        predicted velocity at the next time step can be calculated by solving:
            dv/dt = alpha * v(t) ** 2,
        which gives:
            v(Dt) = v(0) / (1 + alpha * Dt * v(0)).
        Since:
            F_D = m * dv/dt,
        and:
            v(Dt) = v(0) + dv/dt * Dt,
        then:
            F_D = m * (v(Dt) - v(0)) / Dt.
        The drag force calculated above is different from the drag force directly calculated, because the former is the
        amount of force, given the time step, required to reach the velocity predicted at the end of the time step.
        :param rigid_body: a rigid body
        :param delta_time: (s) time between two physics steps
        :return: (N) the drag force
        """
        velocity = rigid_body.velocity.to_array()
        velocity_norm = np.linalg.norm(velocity)
        velocity_next = velocity_norm \
            / (
                    1
                    + 0.5 * self.fluid_density
                    * rigid_body.drag_coefficient * rigid_body.collider.cross_section
                    * delta_time / rigid_body.mass * velocity_norm
            )

        velocity_angle = np.arctan2(rigid_body.velocity.y, rigid_body.velocity.x)

        velocity_next = np.array([
            velocity_next * np.cos(velocity_angle),
            velocity_next * np.sin(velocity_angle)
        ])

        drag = rigid_body.mass * (velocity_next - velocity) / delta_time

        return drag

    def _get_terminal_velocity(self, rigid_body, gravity):
        return np.sqrt(2 * rigid_body.mass * gravity
                       / (self.fluid_density * rigid_body.cross_section * rigid_body.drag_coefficient))

    def update(self, rigid_body, delta_time=None, fluid_density=None, accurate=True):
        """
        Apply the drag force on the rigid body.
        :param rigid_body: a rigid body
        :param delta_time: (s) time between two physics steps
        :param fluid_density: (kg.m-3) density of the fluid around the rigid body
        :param accurate: if True, calculate the drag using the integral formulae (prevent weird behaviours)
        """
        if fluid_density is not None:
            self.fluid_density = fluid_density

        if not accurate:
            drag = self._get_drag(rigid_body)
        else:
            if delta_time is not None and delta_time > 0:
                drag = self._get_drag_accurate(rigid_body, delta_time)
            else:
                drag = np.zeros(2)

        self.x = drag[0]
        self.y = drag[1]


class RocketForce(Force):
    """
    Rocket-like force. Active for a given amount of time (fuel_time), then is 0.
    """

    def __init__(self, vector_2d, fuel_time):
        super().__init__(vector_2d)

        self.fuel_time = fuel_time

    def update(self, rigid_body, delta_time=None):
        """
        Update the force based on the rigid body parameters.
        :param rigid_body: a rigid body
        :param delta_time: (s) physics delta time
        """
        if self.fuel_time > 0:
            self.fuel_time -= delta_time
        else:
            self.x = 0
            self.y = 0


class Weight(Force):
    """
    Weight force.
    """

    def __init__(self, gravity):
        super().__init__(Vector2D(0, 0))

        self._update_gravity(gravity)

    def _get_weight(self, rigid_body):
        return rigid_body.mass * self.gravity

    def _update_gravity(self, gravity):
        if not isinstance(gravity, np.ndarray):
            if np.size(gravity) == 1:
                if not hasattr(gravity, '__iter__'):
                    gravity = [gravity]

                gravity = np.array([0, gravity[0]])
            elif isinstance(gravity, Vector2D):
                gravity = np.array([gravity.x, gravity.y])

        self.gravity = gravity

    def update(self, rigid_body, delta_time=None, gravity=None):
        if gravity is not None:
            self._update_gravity(gravity)

        weight = self._get_weight(rigid_body)

        self.x = weight[0]
        self.y = weight[1]


class RigidBody:
    def __init__(self, collider=None, position=None, velocity=None, forces=None, acceleration=None, mass=1,
                 bounce_coefficient=0.1, drag_coefficient=0.47, power_coefficient=0.1,
                 bounce_enabled=False, is_trigger=False,
                 world=None, use_world_gravity=False, use_world_drag=False,
                 message_trigger_on=None, message_trigger_off=False):
        """
        Initialize a rigid body.
        :param collider: shape of the body, determine its cross-section and its hit box
        :param position: (m) 2D-vector (x, y) representing the position of the center of the body
        :param velocity: (m.s-1) 2D-vector (x, y) representing the speed of the body
        :param forces: (N) list of 2D-vectors (x, y) representing the forces applied to the body
        :param acceleration: (m.s-2) 2D-vector (x, y) representing the intrinsic acceleration of the body
        :param mass: (kg) mass of the body
        :param bounce_coefficient: between 0 and 1, how much speed is conserved after bouncing
        :param drag_coefficient: how much the object is affected by drag, depends on its shape
        :param power_coefficient: conversion factor between the "power" and the actual velocity
        :param bounce_enabled: if True, the object can bounce
        :param is_trigger: if True, the object can be used as a trigger
        :param world: world in which the object exists
        :param use_world_gravity: if True, add the weight of the body according to the world gravity
        :param use_world_drag: if True, add the drag of the body according to the world fluid density
        :param message_trigger_on: message to display when trigger is activated
        :param message_trigger_off: message to display when trigger is deactivated
        """
        # Prevent division by 0 because of mass when applying forces
        if np.abs(mass) <= np.finfo(float).tiny:
            raise ValueError('mass of RigidBody must be non-zero')

        if collider is None:
            collider = Sphere(0.0)

        if position is None:
            position = Vector2D(0, 0)
        elif np.size(position) == 2:
            position = Vector2D(position[0], position[1])

        if velocity is None:
            velocity = Vector2D(0, 0)
        elif np.size(velocity) == 2:
            velocity = Vector2D(velocity[0], velocity[1])

        if acceleration is None:
            acceleration = Vector2D(0, 0)
        elif np.size(acceleration) == 2:
            acceleration = Vector2D(acceleration[0], acceleration[1])

        self.collider = collider

        self.position = position
        self.velocity = velocity
        self.acceleration = acceleration
        self.mass = mass

        self.bounce_coefficient = bounce_coefficient
        self.drag_coefficient = drag_coefficient
        self.power_coefficient = power_coefficient
        self.bounce_enabled = bounce_enabled
        self.is_trigger = is_trigger
        self.trigger = False

        self.use_world_gravity = use_world_gravity
        self.use_world_drag = use_world_drag

        self.message_trigger_on = message_trigger_on
        self.message_trigger_off = message_trigger_off

        self.forces = forces

        self.set_forces(
            forces=self.forces,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag
        )

    def sum_forces(self, delta_time):
        """
        Return the sum of the forces applied to the body. Uses numpy arrays for performances.
        """
        if self.forces.size == 0:
            return Vector2D(0, 0)

        for i, force in enumerate(self.forces):
            self.forces[i].update(rigid_body=self, delta_time=delta_time)

        sum_forces = np.sum(np.asarray([
            list(map(lambda a: a.x, self.forces)),
            list(map(lambda a: a.y, self.forces))
        ]), axis=1)

        return Vector2D.from_array(sum_forces)

    @staticmethod
    def _reflect(speed, normal):
        """
        Change in velocity after an object colliding with an immovable surface.
        Source: https://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector
        Given the velocity vector v and the normal n:
            - v_n the projection of v on n is norm(v) * cos(a), a is the angle between v and n
            - this is equivalent to v_n = norm(v) * norm(n) * cos(a) = dot(v, n) since norm(n) = 1
            - to get the direction of v_n, we multiply by n -> v_n_ = dot(v, n) * n
            - v can be written as the sum of two perpendicular vectors: v = w + v_n_
            - w = v - v_n_
            - r the reflected velocity vector has the same w (perpendicular) than v, but inverted v_n_ (parallel)
            - r = w - v_n_ = v - v_n_ - v_n_ = v - 2 * dot(v, n) * n
        :param speed: speed vector
        :param normal: normal of the surface, must be normalized
        :return:
        """
        velocity = np.array([speed.x, speed.y])
        return velocity - 2 * np.dot(velocity, normal) * normal

    def bounce_world(self, world, delta_time):
        """
        Rules to bounce the body on other objects.
        :param world: a world object
        :param delta_time: (s) time between two time steps in the world
        :return:
        """
        next_x = self.position.x + self.velocity.x * delta_time
        next_y = self.position.y + self.velocity.y * delta_time

        if isinstance(self.collider, Sphere):
            if next_x - self.collider.radius < world.boundaries[0, 0] \
                    or next_x + self.collider.radius > world.boundaries[1, 0]:
                self.velocity.x = -self.velocity.x * self.bounce_coefficient
            elif next_y - self.collider.radius < world.boundaries[0, 1] \
                    or next_y + self.collider.radius > world.boundaries[1, 1]:
                self.velocity.y = -self.velocity.y * self.bounce_coefficient
        elif isinstance(self.collider, Box):
            if next_x < world.boundaries[0, 0] \
                    or next_x + self.collider.size.x > world.boundaries[1, 0]:
                self.velocity.x = -self.velocity.x * self.bounce_coefficient
            elif next_y < world.boundaries[0, 1] \
                    or next_y + self.collider.size.y > world.boundaries[1, 1]:
                self.velocity.y = -self.velocity.y * self.bounce_coefficient

    def get_angle(self):
        """
        Return the angle of the shot from the velocity of the object.
        """
        return np.round(np.rad2deg(np.arctan2(self.velocity.y, self.velocity.x)))

    def get_power(self):
        """
        Return the power of the shot from the velocity of the object.
        """
        if self.velocity.x ** 2 + self.velocity.y ** 2 <= 0:
            return 0
        else:
            return np.round((self.velocity.x ** 2 + self.velocity.y ** 2) / self.power_coefficient)

    def move(self, world, delta_time):
        """
        Rules to move the body inside the world.
        :param world: world in which the body is evolving
        :param delta_time: (s) time between two time steps in the world
        """
        self.update_velocity(delta_time, self.sum_forces(delta_time))

        if self.bounce_enabled:
            if world.bounce_enabled:
                self.bounce_world(world, delta_time)

            for body in world.objects:
                if body.rigid_body != self:
                    new_velocity = self.collider.bounce(self, body.rigid_body, delta_time)

                    if body.rigid_body.is_trigger and self.collider.other_collider == body.rigid_body.collider:
                        if not body.rigid_body.trigger:
                            body.rigid_body.on_triggered()

                    if body.rigid_body.bounce_enabled:
                        self.velocity = new_velocity

        self.position = self.update_position(delta_time)

    def on_triggered(self, message_on=None, message_off=None):
        """
        Action to do if the body is triggered.
        :param message_on: message to print is the trigger of the body is activated
        :param message_off: message to print is the trigger of the body is deactivated
        """
        if message_on is None:
            message_on = self.message_trigger_on

        if message_off is None:
            message_off = self.message_trigger_off

        if not self.trigger:
            if message_on is not None:
                print(message_on)

            self.trigger = True
        elif message_off is not None:
            print(message_off)

            self.trigger = False

    def power2velocity(self, power, max_power):
        """
        Return the norm of the velocity from the power of the shot.
        :param power: power of the shot in arbitrary units
        :param max_power: maximum power of the shot in arbitrary units
        :return: (m.s-1) the velocity of the shot
        """
        if power > max_power:
            power = max_power

        return np.sqrt(power * self.power_coefficient)

    def set_forces(self, forces: np.ndarray[Force] = None,
                   world: World = None, use_world_gravity: bool = False, use_world_drag: bool = False):
        """
        Set the forces applied to the body.
        :param forces: forces applying only on the body
        :param world: world of the body
        :param use_world_gravity: if True, add the weight of the body to the forces applying to it
        :param use_world_drag: if True, add the drag of the body to the forces applying to it
        """
        if forces is None:
            forces = np.array([], dtype=object)
        else:
            for force in forces:
                force.update(rigid_body=self)

        if world is not None:
            if use_world_gravity:
                weight = Weight(gravity=-world.gravity)
                weight.update(rigid_body=self)
                forces = np.concatenate((forces, (weight,)))

            if use_world_drag:
                drag = Drag(fluid_density=world.fluid_density)
                drag.update(rigid_body=self)
                forces = np.concatenate((forces, (drag,)))

        self.forces = forces

    def set_velocity_from_power(self, power=None, angle=None, max_power=100):
        """
        Set the velocity from the power and the angle of the shot.
        :param power: power of the shot, arbitrary units
        :param angle: (deg) angle of the shot
        :param max_power: maximum possible power
        """
        if power is None:
            power = self.get_power()

        if angle is None:
            angle = self.get_angle()

        velocity = self.power2velocity(power, max_power)

        self.velocity.x = velocity * np.cos(np.deg2rad(angle))
        self.velocity.y = velocity * np.sin(np.deg2rad(angle))

    def update_velocity(self, delta_time, sum_forces):
        self.acceleration = sum_forces
        self.acceleration.x /= self.mass
        self.acceleration.y /= self.mass

        self.velocity.x += self.acceleration.x * delta_time
        self.velocity.y += self.acceleration.y * delta_time

    def update_position(self, delta_time):
        next_position = Vector2D(self.position.x, self.position.y)
        next_position.x += self.velocity.x * delta_time
        next_position.y += self.velocity.y * delta_time

        return next_position
