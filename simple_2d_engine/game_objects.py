"""
Game objects.
"""
import matplotlib.patches
import numpy as np

from simple_2d_engine.colliders import Box, Edge, Sphere, Vector2D
from simple_2d_engine.physics import RigidBody, RocketForce
from simple_2d_engine.world import World


# Unplayable game objects
class GameObject:
    def __init__(self, rigid_body: RigidBody, sprite: matplotlib.patches.Patch):
        self.rigid_body = rigid_body
        self.sprite = sprite

    def move(self, world: World, delta_time):
        self.rigid_body.move(world, delta_time)

        if isinstance(self.sprite, matplotlib.patches.Circle):
            self.sprite.center = (self.rigid_body.position.x, self.rigid_body.position.y)
        else:
            self.sprite.xy = (self.rigid_body.position.x, self.rigid_body.position.y)

    @classmethod
    def new_rigid_sphere(
            cls, radius,
            position=None, velocity=None, forces=None, acceleration=None, mass=1,
            bounce_coefficient=0.1, drag_coefficient=0.47, power_coefficient=0.1,
            bounce_enabled=False, is_trigger=False,
            world=None, use_world_gravity=False, use_world_drag=False,
            color='k', linewidth=1, fill=False
    ):
        collider = Sphere(radius)
        rigid_body = RigidBody(
            position=position,
            velocity=velocity,
            collider=collider,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            power_coefficient=power_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
        )
        sprite = matplotlib.patches.Circle(
            xy=(rigid_body.position.x, rigid_body.position.y),
            radius=rigid_body.collider.radius,
            color=color,
            linewidth=linewidth,
            fill=fill
        )

        return cls(rigid_body, sprite)

    @classmethod
    def new_rigid_box(
            cls, size, rotation=0,
            position=None, velocity=None, forces=None, acceleration=None, mass=1,
            bounce_coefficient=0.1, drag_coefficient=0.47, bounce_enabled=False, is_trigger=False,
            world=None, use_world_gravity=False, use_world_drag=False,
            color='k', linewidth=1, fill=False
    ):
        collider = Box(size, rotation)
        rigid_body = RigidBody(
            position=position,
            velocity=velocity,
            collider=collider,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
        )
        sprite = matplotlib.patches.Rectangle(
            xy=(rigid_body.position.x, rigid_body.position.y),
            width=rigid_body.collider.size.x,
            height=rigid_body.collider.size.y,
            angle=rigid_body.collider.rotation.z,
            color=color,
            linewidth=linewidth,
            fill=fill
        )

        return cls(rigid_body, sprite)

    @classmethod
    def new_rigid_edge(
            cls, length, rotation,
            position=None, velocity=None, forces=None, acceleration=None, mass=1,
            bounce_coefficient=0.1, drag_coefficient=0.47, bounce_enabled=False, is_trigger=False,
            world=None, use_world_gravity=False, use_world_drag=False,
            color='k', linewidth=1, fill=False
    ):
        collider = Edge(length, rotation)
        rigid_body = RigidBody(
            position=position,
            velocity=velocity,
            collider=collider,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
        )
        sprite = matplotlib.patches.Rectangle(
            xy=(rigid_body.position.x, rigid_body.position.y),
            width=rigid_body.collider.length,
            height=0,
            angle=rigid_body.collider.rotation.z,
            color=color,
            linewidth=linewidth,
            fill=fill
        )

        return cls(rigid_body, sprite)


class ObstacleEdge(GameObject):
    default_color = 'k'
    default_linewidth = 3

    def __init__(self, length, rotation, position, velocity=None, forces=None, acceleration=None, mass=1,
                 bounce_coefficient=None, drag_coefficient=None, bounce_enabled=True, is_trigger=False, world=None,
                 use_world_gravity=False, use_world_drag=False, color=None, linewidth=None, fill=False):
        if velocity is None:
            velocity = (0, 0)

        if color is None:
            color = ObstacleEdge.default_color

        if linewidth is None:
            linewidth = HockeyPuck.default_linewidth

        game_object = GameObject.new_rigid_edge(
            length,
            rotation,
            position=position,
            velocity=velocity,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
            color=color,
            linewidth=linewidth,
            fill=fill
        )

        super().__init__(game_object.rigid_body, game_object.sprite)

        del game_object


class ObstacleBox(GameObject):
    default_color = 'k'
    default_linewidth = 3

    def __init__(self, size, position, rotation=0, velocity=None, forces=None, acceleration=None, mass=1,
                 bounce_coefficient=None, drag_coefficient=None, bounce_enabled=True, is_trigger=False, world=None,
                 use_world_gravity=False, use_world_drag=False, color=None, linewidth=None, fill=False):
        if velocity is None:
            velocity = (0, 0)

        if color is None:
            color = ObstacleEdge.default_color

        if linewidth is None:
            linewidth = HockeyPuck.default_linewidth

        game_object = GameObject.new_rigid_box(
            size,
            rotation=rotation,
            position=position,
            velocity=velocity,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
            color=color,
            linewidth=linewidth,
            fill=fill
        )

        super().__init__(game_object.rigid_body, game_object.sprite)

        del game_object


class WinEdge(GameObject):
    default_color = 'gold'
    default_linewidth = 3

    def __init__(self, length, rotation, position, velocity=None, forces=None, acceleration=None, mass=1,
                 bounce_coefficient=None, drag_coefficient=None, bounce_enabled=False, is_trigger=True, world=None,
                 use_world_gravity=False, use_world_drag=False, color=None, linewidth=None, fill=False):
        if velocity is None:
            velocity = (0, 0)

        if color is None:
            color = WinEdge.default_color

        if linewidth is None:
            linewidth = HockeyPuck.default_linewidth

        game_object = GameObject.new_rigid_edge(
            length,
            rotation,
            position=position,
            velocity=velocity,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
            color=color,
            linewidth=linewidth,
            fill=fill
        )

        super().__init__(game_object.rigid_body, game_object.sprite)

        self.rigid_body.message_trigger_on = "You win!"

        del game_object


# Playable game objects
class PlayableObject(GameObject):
    default_radius = 1.0
    default_mass = 1.0
    default_bounce_coefficient = 0.9
    default_drag_coefficient = 0.47
    default_power_coefficient = 0.1
    default_color = 'r'
    default_linewidth = 3

    def __init__(self, radius=None, position=None, velocity=None, forces=None, acceleration=None, mass=None,
                 bounce_coefficient=None, drag_coefficient=None, power_coefficient=None,
                 bounce_enabled=True, is_trigger=False, world=None,
                 use_world_gravity=True, use_world_drag=True, color=None, linewidth=None, fill=False):
        radius, position, velocity, mass, bounce_coefficient, drag_coefficient, power_coefficient, color, linewidth = \
            self._set_default_attributes(
                radius, position, velocity, mass, bounce_coefficient, drag_coefficient, power_coefficient,
                color, linewidth
            )

        game_object = GameObject.new_rigid_sphere(
            radius,
            position=position,
            velocity=velocity,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            power_coefficient=power_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
            color=color,
            linewidth=linewidth,
            fill=fill
        )

        super().__init__(
            rigid_body=game_object.rigid_body,
            sprite=game_object.sprite
        )

        del game_object

    def _set_default_attributes(self, radius, position, velocity, mass,
                                bounce_coefficient, drag_coefficient, power_coefficient,
                                color, linewidth):
        if radius is None:
            radius = self.default_radius

        if position is None:
            position = (0, 0)

        if velocity is None:
            velocity = (0, 0)

        if mass is None:
            mass = self.default_mass

        if bounce_coefficient is None:
            bounce_coefficient = self.default_bounce_coefficient

        if drag_coefficient is None:
            drag_coefficient = self.default_drag_coefficient

        if power_coefficient is None:
            power_coefficient = self.default_power_coefficient

        if color is None:
            color = self.default_color

        if linewidth is None:
            linewidth = self.default_linewidth

        return radius, position, velocity, mass, bounce_coefficient, drag_coefficient, power_coefficient, \
            color, linewidth


class Apple(PlayableObject):
    default_radius = 6e-2
    default_mass = 100e-3
    default_bounce_coefficient = 0.05
    default_drag_coefficient = 0.47
    default_color = 'r'
    default_linewidth = 3

    def __init__(self, radius=None, position=None, velocity=None, forces=None, acceleration=None, mass=None,
                 bounce_coefficient=None, drag_coefficient=None, power_coefficient=None,
                 bounce_enabled=True, is_trigger=False, world=None,
                 use_world_gravity=True, use_world_drag=True, color=None, linewidth=None, fill=False):
        super().__init__(
            radius=radius,
            position=position,
            velocity=velocity,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            power_coefficient=power_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
            color=color,
            linewidth=linewidth,
            fill=fill
        )


class HockeyPuck(PlayableObject):
    default_radius = 76e-3 / 2
    default_mass = 170e-3
    default_bounce_coefficient = 0.9
    default_drag_coefficient = 1.17
    default_color = 'r'
    default_linewidth = 3

    def __init__(self, radius=None, position=None, velocity=None, forces=None, acceleration=None, mass=None,
                 bounce_coefficient=None, drag_coefficient=None, power_coefficient=None,
                 bounce_enabled=True, is_trigger=False, world=None,
                 use_world_gravity=True, use_world_drag=True, color=None, linewidth=None, fill=False):
        super().__init__(
            radius=radius,
            position=position,
            velocity=velocity,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            power_coefficient=power_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
            color=color,
            linewidth=linewidth,
            fill=fill
        )


class Missile(PlayableObject):
    default_radius = 8e-2
    default_mass = 29.0
    default_bounce_coefficient = 0.1
    default_drag_coefficient = 0.295
    default_color = 'r'
    default_linewidth = 3
    default_thrust = 3000.0
    default_fuel_time = 2.0
    default_trail_color = 'grey'

    def __init__(self, radius=None, position=None, velocity=None, forces=None, acceleration=None, mass=None,
                 bounce_coefficient=None, drag_coefficient=None, power_coefficient=None,
                 bounce_enabled=True, is_trigger=False, world=None,
                 use_world_gravity=True, use_world_drag=True, color=None, linewidth=None, fill=False,
                 thrust=None, fuel_time=None, trail_color=None):

        super().__init__(
            radius=radius,
            position=position,
            velocity=velocity,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            power_coefficient=power_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
            linewidth=linewidth,
            fill=fill
        )

        if thrust is None:
            thrust = self.default_thrust

        if fuel_time is None:
            fuel_time = self.default_fuel_time

        if np.linalg.norm(self.rigid_body.velocity.to_array()) <= 0:
            rocket_force = RocketForce(Vector2D(0.0, thrust), fuel_time)
        else:
            initial_velocity = self.rigid_body.velocity.to_array()
            rocket_force = initial_velocity / np.linalg.norm(initial_velocity) * thrust
            rocket_force = RocketForce(Vector2D.from_array(rocket_force), fuel_time)

        if color is None:
            color = self.default_color

        if trail_color is None:
            trail_color = self.default_trail_color

        self.missile_color = color
        self.sprite.set_edgecolor(trail_color)  # by default, use the trail color

        self.rigid_body.forces.append(
            rocket_force
        )
        self.rocket_force_id = len(self.rigid_body.forces) - 1  # Get the index of the rocket force
        self.has_fuel_left = True

    def move(self, world, delta_time):
        # Manage trail color
        if self.rigid_body.forces[self.rocket_force_id].fuel_time <= 0 and self.has_fuel_left:
            self.sprite.set_edgecolor(self.missile_color)
            self.has_fuel_left = False

        # Object movement
        self.rigid_body.move(world, delta_time)
        self.sprite.center = (self.rigid_body.position.x, self.rigid_body.position.y)

    def update_rocket_force(self):
        """
        Initialize the angle of the rocket force from the initial velocity of the rigid body.
        """
        for i, force in enumerate(self.rigid_body.forces):
            if isinstance(force, RocketForce):
                thrust = np.linalg.norm(force.to_array())
                fuel_time = force.fuel_time

                if np.linalg.norm(self.rigid_body.velocity.to_array()) <= 0:
                    rocket_force = RocketForce(Vector2D(0.0, thrust), fuel_time)
                else:
                    initial_velocity = self.rigid_body.velocity.to_array()
                    rocket_force = initial_velocity / np.linalg.norm(initial_velocity) * thrust
                    rocket_force = RocketForce(Vector2D.from_array(rocket_force), fuel_time)

                self.rigid_body.forces[i] = rocket_force


class Parachutist(PlayableObject):
    default_radius = 0.15  # (m) without parachute
    default_mass = 75  # (kg)
    default_bounce_coefficient = 0.1
    default_drag_coefficient = 1.00  # without parachute
    default_parachute_radius = 1.0  # (m)
    default_parachute_open_time = 10.0  # (s)
    default_parachute_drag_coefficient = 1.2
    default_color = 'r'
    default_linewidth = 1

    def __init__(self, radius=None, position=None, velocity=None, forces=None, acceleration=None, mass=None,
                 bounce_coefficient=None, drag_coefficient=None, power_coefficient=None,
                 bounce_enabled=True, is_trigger=False, world=None,
                 use_world_gravity=True, use_world_drag=True, color=None, linewidth=None, fill=False,
                 parachute_radius=None, parachute_open_time=None, parachute_drag_coefficient=None,
                 display_landing_speed=False):
        super().__init__(
            radius=radius,
            position=position,
            velocity=velocity,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            power_coefficient=power_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
            color=color,
            linewidth=linewidth,
            fill=fill
        )

        if parachute_radius is None:
            parachute_radius = self.default_parachute_radius

        if parachute_open_time is None:
            parachute_open_time = self.default_parachute_open_time

        if parachute_drag_coefficient is None:
            parachute_drag_coefficient = self.default_parachute_drag_coefficient

        self.parachute_radius = parachute_radius
        self.parachute_open_time = parachute_open_time
        self.parachute_drag_coefficient = parachute_drag_coefficient

        self.display_landing_speed = display_landing_speed

        self.timer = 0.0
        self.parachute_is_open = False

    def move(self, world, delta_time):
        # Manage parachute opening
        if self.timer < self.parachute_open_time:
            if self.timer == 0:
                self.update_rigid_body_move_function()

            self.timer += delta_time
        elif not self.parachute_is_open:
            self.parachute_is_open = True
            self.sprite.radius = self.parachute_radius

            parachutist_radius = self.rigid_body.collider.radius

            # Get parachute cross-section for the drag force
            self.rigid_body.collider.radius = self.parachute_radius
            self.rigid_body.collider.cross_section = self.rigid_body.collider.get_cross_section()
            self.rigid_body.drag_coefficient = self.parachute_drag_coefficient

            # Use the parachutist radius for collision
            self.rigid_body.collider.radius = parachutist_radius

            self.update_rigid_body_move_function()

        # Object movement
        self.rigid_body.move(world, delta_time)
        self.sprite.center = (self.rigid_body.position.x, self.rigid_body.position.y)

    def update_rigid_body_move_function(self):
        self.rigid_body.move = lambda w=None, dt=None: \
            Parachutist.rigid_body_move(self.rigid_body, w, dt, self.display_landing_speed)

    @staticmethod
    def rigid_body_move(rigid_body, world, delta_time, display_landing_speed):
        """
        Rules to move the body inside the world.
        :param rigid_body: rigid body of the game object
        :param world: world in which the body is evolving
        :param delta_time: (s) time between two time steps in the world
        :param display_landing_speed: if True, display the speed when triggering
        """
        rigid_body.update_velocity(delta_time, rigid_body.sum_forces(delta_time))

        if rigid_body.bounce_enabled:
            if world.bounce_enabled:
                rigid_body.bounce_world(world, delta_time)

            for body in world.objects:
                if body.rigid_body != rigid_body:
                    new_velocity = rigid_body.collider.bounce(rigid_body, body.rigid_body, delta_time)

                    if body.rigid_body.is_trigger and rigid_body.collider.other_collider == body.rigid_body.collider:
                        if display_landing_speed and not body.rigid_body.trigger:
                            print(f'Landing speed: {np.linalg.norm(rigid_body.velocity.to_array()): .2f} m/s')

                        if not body.rigid_body.trigger:
                            body.rigid_body.on_triggered()

                    if body.rigid_body.bounce_enabled:
                        rigid_body.velocity = new_velocity

        rigid_body.position = rigid_body.update_position(delta_time)


class Shell(PlayableObject):
    default_radius = 6e-2
    default_mass = 14.3
    default_bounce_coefficient = 0.1
    default_drag_coefficient = 0.295
    default_color = 'r'
    default_linewidth = 3

    def __init__(self, radius=None, position=None, velocity=None, forces=None, acceleration=None, mass=None,
                 bounce_coefficient=None, drag_coefficient=None, power_coefficient=None,
                 bounce_enabled=True, is_trigger=False, world=None,
                 use_world_gravity=True, use_world_drag=True, color=None, linewidth=None, fill=False):
        super().__init__(
            radius=radius,
            position=position,
            velocity=velocity,
            forces=forces,
            acceleration=acceleration,
            mass=mass,
            bounce_coefficient=bounce_coefficient,
            drag_coefficient=drag_coefficient,
            power_coefficient=power_coefficient,
            bounce_enabled=bounce_enabled,
            is_trigger=is_trigger,
            world=world,
            use_world_gravity=use_world_gravity,
            use_world_drag=use_world_drag,
            color=color,
            linewidth=linewidth,
            fill=fill
        )
