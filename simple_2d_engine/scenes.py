"""
Contains the levels and menus of the game.
"""
import copy

import matplotlib.pyplot as plt
import numpy as np

from simple_2d_engine import game_objects
from simple_2d_engine.animation import play_animation
from simple_2d_engine.world import World


class Scene:
    def __init__(self, world: World,
                 objects: np.ndarray[game_objects.GameObject] | list[game_objects.GameObject],
                 n_frames: int = 150, activate_trace: bool = False, title: str = 'A level'):
        """Scene object.
        Each level consists of a scene.
        :param world: the World to use for the scene
        :param objects: list of GameObjects to add to the scene (e.g., as obstacles)
        :param n_frames: number of frames of the scene; the scene ends if the win edge has not been triggered by then
        :param activate_trace: if True, game objects will leave a trace behind them as they move through the World
        :param title: the title of the scene
        """
        self.activate_trace = activate_trace
        self.n_frames = n_frames
        self.objects = objects
        self.title = title
        self.world = world

    def load(self, reset_forces=True):
        """Initialize the scene world."""
        if reset_forces:
            for obj in self.objects:
                obj.rigid_body.set_forces(
                    forces=obj.rigid_body.forces,
                    world=self.world,
                    use_world_gravity=obj.rigid_body.use_world_gravity,
                    use_world_drag=obj.rigid_body.use_world_drag
                )

        self.world.objects = np.append(self.world.objects, self.objects)

        return self.world, self.n_frames, self.title, self.activate_trace

    def look(self):
        animation, trace_data_x, trace_data_y, time, delta_time, fig, ax = self.setup_look()
        self.pyplot_show(ax)

        return animation, trace_data_x, trace_data_y, time, delta_time, fig, ax

    def play(self):
        animation, trace_data_x, trace_data_y, time, delta_time, fig, ax = self.setup_play()
        self.pyplot_show(ax)

        return animation, trace_data_x, trace_data_y, time, delta_time, fig, ax

    def pyplot_show(self, ax, world=None):
        """Ensure axes are plotted correctly"""
        if world is None:
            world = self.world

        ax.set_aspect(1)
        ax.set_xlim(world.boundaries[:, 0])
        ax.set_ylim(world.boundaries[:, 1])
        ax.set_navigate(False)
        plt.show()

    def setup_look(self, world: World = None, x_lim=None, y_lim=None):
        """Setup to look at the scene for 1 frame."""
        if world is None:
            world = self.world

        if x_lim is None:
            x_lim = world.boundaries[:, 0]

        if y_lim is None:
            y_lim = world.boundaries[:, 1]

        return play_animation(
            world=world,
            n_frames=1,
            x_lim=x_lim,
            y_lim=y_lim
        )

    def setup_play(self, world: World = None, n_frames: int = None, time_interval=0.03, speed=1,
                   x_lim=None, y_lim=None,
                   trace=None, trace_color=None, trace_line_style=None,
                   save_to_js_html=False, animation_file='./movie.html'):
        """Setup to play the scene."""
        if world is None:
            world = self.world

        if x_lim is None:
            x_lim = world.boundaries[:, 0]

        if y_lim is None:
            y_lim = world.boundaries[:, 1]

        if n_frames is None:
            n_frames = self.n_frames

        if trace is None:
            trace = self.activate_trace

        return play_animation(
            world=world,
            n_frames=n_frames,
            time_interval=time_interval,
            speed=speed,
            x_lim=x_lim,
            y_lim=y_lim,
            trace=trace,
            trace_color=trace_color,
            trace_line_style=trace_line_style,
            save_to_js_html=save_to_js_html,
            animation_file=animation_file
        )


class GameScene(Scene):
    def __init__(self, world: World,
                 playable_object: game_objects.GameObject, win_edge: game_objects.WinEdge,
                 objects: np.ndarray[game_objects.GameObject] | list[game_objects.GameObject] = None,
                 n_frames: int = 150, activate_trace: bool = False, title: str = 'A level'):
        """Scene object.
        Each level consists of a scene.
        :param world: the World to use for the scene
        :param playable_object: the object controlled by the player
        :param win_edge: the edge that the playable object must trigger for the player to win the level
        :param objects: list of GameObjects to add to the scene (e.g., as obstacles)
        :param n_frames: number of frames of the scene; the scene ends if the win edge has not been triggered by then
        :param activate_trace: if True, game objects will leave a trace behind them as they move through the World
        :param title: the title of the scene
        """
        self.playable_object = playable_object
        self.win_edge = win_edge

        super().__init__(
            world=world,
            objects=objects,
            n_frames=n_frames,
            activate_trace=activate_trace,
            title=title
        )

    def load(self, reset_forces=True):
        """Initialize the scene world."""
        if self.objects is not None:
            objects = np.append(self.objects, [self.playable_object, self.win_edge])
        else:
            objects = np.array([self.playable_object, self.win_edge])

        if reset_forces:
            for obj in objects:
                obj.rigid_body.set_forces(
                    forces=obj.rigid_body.forces,
                    world=self.world,
                    use_world_gravity=obj.rigid_body.use_world_gravity,
                    use_world_drag=obj.rigid_body.use_world_drag
                )

        self.world.objects = np.append(self.world.objects, objects)

        return self.world, self.playable_object, self.win_edge, self.n_frames, self.title, self.activate_trace


def load_default_scenes():
    return [
        GameScene(  # level 1; solution: p 100 a 0
            title='First strike!',
            world=World(
                boundaries=np.array([[0, 0], [2, 2]]),
                gravity=0,
                bounce_enabled=False
            ),
            playable_object=game_objects.HockeyPuck(
                position=(1, 1)
            ),
            win_edge=game_objects.WinEdge(
                length=0.5,
                rotation=90,
                position=(1.9, 0.75)
            )
        ),
        GameScene(  # level 2; solution: p 100 a 33
            title='Angle your shot!',
            world=World(
                boundaries=np.array([[0, 0], [2, 2]]),
                gravity=0,
                bounce_enabled=False
            ),
            playable_object=game_objects.HockeyPuck(
                position=(1, 1)
            ),
            win_edge=game_objects.WinEdge(
                length=0.25,
                rotation=90,
                position=(1.9, 1.5),
            )
        ),
        GameScene(  # level 3; solution: p 100 a 68
            title='Bounce around the obstacle!',
            world=World(
                boundaries=np.array([[0, 0], [2, 2]]),
                gravity=0,
                bounce_enabled=True
            ),
            playable_object=game_objects.HockeyPuck(
                position=(0.5, 1)
            ),
            win_edge=game_objects.WinEdge(
                length=1.0,
                rotation=90,
                position=(1.9, 0.5)
            ),
            objects=[
                game_objects.ObstacleEdge(  # vertical edge
                    length=1.25,
                    rotation=90,
                    position=(1, 0.375)
                ),
                game_objects.ObstacleEdge(  # bottom horizontal edge
                    length=0.01,
                    rotation=0,
                    position=(0.995, 0.375),
                    color='b'
                ),
                game_objects.ObstacleEdge(  # top horizontal edge
                    length=0.01,
                    rotation=0,
                    position=(0.995, 1.625),
                    color='r'
                )
            ]
        ),
        GameScene(  # level 4; solution: p 80 a 45
            title='Jump above the hole!',
            world=World(
                boundaries=np.array([[0, 0], [5, 5]]),
                gravity=9.81,
                fluid_density=0,
                bounce_enabled=False
            ),
            playable_object=game_objects.Apple(
                position=(1.99, 2.15),
                power_coefficient=0.25
            ),
            win_edge=game_objects.WinEdge(
                length=0.5,
                rotation=0,
                position=(3.75, 1.95),
                bounce_enabled=False
            ),
            objects=[
                game_objects.ObstacleEdge(  # surface 1
                    length=2,
                    rotation=0,
                    position=(0, 2)
                ),
                game_objects.ObstacleEdge(  # surface 2
                    length=0.75,
                    rotation=0,
                    position=(3, 2),
                ),
                game_objects.ObstacleEdge(  # surface 3
                    length=10,
                    rotation=0,
                    position=(4.25, 2)
                ),
                game_objects.ObstacleEdge(  # hole 1
                    length=0.1,
                    rotation=-90,
                    position=(3.75, 2)
                ),
                game_objects.ObstacleEdge(  # hole 2
                    length=0.1,
                    rotation=-90,
                    position=(4.25, 2)
                ),
                game_objects.ObstacleEdge(  # hole bottom
                    length=0.5,
                    rotation=0,
                    position=(3.75, 1.90)
                ),
                game_objects.ObstacleEdge(  # big hole 1
                    length=2,
                    rotation=-90,
                    position=(2, 2)
                ),
                game_objects.ObstacleEdge(  # big hole 2
                    length=2,
                    rotation=-90,
                    position=(3, 2)
                )
            ],
            activate_trace=True
        ),
        GameScene(  # level 5; solution: p 100 a -40
            title='Safe drag',
            n_frames=1000,
            world=World(
                boundaries=np.array([[-15, 0], [15, 110]]),
                gravity=9.81,
                fluid_density=1.2,
                bounce_enabled=False
            ),
            playable_object=game_objects.Parachutist(
                position=(0, 100),
                power_coefficient=0.25,
                display_landing_speed=True
            ),
            win_edge=game_objects.WinEdge(
                length=6,
                rotation=0,
                position=(9, 0),
                bounce_enabled=True
            ),
            objects=[
                game_objects.ObstacleEdge(
                    length=30,
                    rotation=0,
                    position=(-15, -0.1),
                    linewidth=0.1
                )
            ]
        ),
        GameScene(  # level 6; solution: p 100 a 26
            title='Cannon!',
            n_frames=1100,
            activate_trace=True,
            world=World(
                boundaries=np.array([[0, 0], [5000, 2500]]),
                gravity=9.81,
                fluid_density=1.2,
                bounce_enabled=False
            ),
            playable_object=game_objects.Shell(
                position=(100.0, 502.0),
                power_coefficient=1e3
            ),
            win_edge=game_objects.WinEdge(
                length=200.0,
                rotation=0,
                position=(4700, 510),
                bounce_enabled=False,
            ),
            objects=[
                game_objects.ObstacleEdge(  # plain 1
                    length=1500,
                    rotation=0,
                    position=(0, 500)
                ),
                game_objects.ObstacleEdge(  # plain 2
                    length=1500,
                    rotation=0,
                    position=(3500, 500)
                ),
                game_objects.ObstacleEdge(  # hill 1
                    length=np.sqrt(200 ** 2 + 1000 ** 2),
                    rotation=np.rad2deg(np.arcsin(200 / 1000)),
                    position=(1500, 500)
                ),
                game_objects.ObstacleEdge(  # hill 2
                    length=np.sqrt(200 ** 2 + 1000 ** 2),
                    rotation=180 - np.rad2deg(np.arcsin(200 / 1000)),
                    position=(3500, 500)
                )
            ]
        ),
        GameScene(  # level 7; solution: p 100 a 154
            title='Strike back!',
            n_frames=1100,
            activate_trace=True,
            world=World(
                boundaries=np.array([[0, 0], [5000, 2500]]),
                gravity=9.81,
                fluid_density=1.2,
                bounce_enabled=False
            ),
            playable_object=game_objects.Shell(
                position=(4800, 502.0),
                power_coefficient=1e3
            ),
            win_edge=game_objects.WinEdge(
                length=200.0,
                rotation=0,
                position=(50, 510),
                bounce_enabled=False
            ),
            objects=[
                game_objects.ObstacleEdge(  # plain 1
                    length=1500,
                    rotation=0,
                    position=(0, 500)
                ),
                game_objects.ObstacleEdge(  # plain 2
                    length=1500,
                    rotation=0,
                    position=(3500, 500)
                ),
                game_objects.ObstacleEdge(  # hill 1
                    length=np.sqrt(200 ** 2 + 1000 ** 2),
                    rotation=np.rad2deg(np.arcsin(200 / 1000)),
                    position=(1500, 500)
                ),
                game_objects.ObstacleEdge(  # hill 2
                    length=np.sqrt(200 ** 2 + 1000 ** 2),
                    rotation=180 - np.rad2deg(np.arcsin(200 / 1000)),
                    position=(3500, 500)
                )
            ]
        )
    ]


def scene_manager(scene_id: int, scenes: list[GameScene] = None):
    """
    Level manager.
    :param scene_id: scene to load
    :param scenes: list of scenes of the game
    """
    print(f'\nStarting level {scene_id}...')

    # Initialization
    animation = None
    trace_data_x = None
    trace_data_y = None
    time = None
    delta_time = None

    scene = copy.deepcopy(scenes[scene_id - 1])

    world, playable_body, win_edge, frames, level_title, activate_trace = scene.load()
    print(f'\nLevel {scene_id}: {level_title}\n')

    print('Initial planning phase')
    _ = scene.look()

    # Main loop
    while not win_edge.rigid_body.trigger:
        print(f'\nLevel {scene_id}: {level_title}')

        # Wait for player input
        choice = input(
            f'\nPower: {int(playable_body.rigid_body.get_power())}\n'
            f'Angle: {int(playable_body.rigid_body.get_angle())}°\n'
            '\nWhat do you want to do ?\n'
            '!: launch !\n'
            'l: look level\n'
            'p: set power and angle\n'
            'r: return to menu\n'
            'quit: quit\n'
        )

        # Execute player action
        if choice == 'r':
            return False
        elif choice == 'quit':
            return True
        elif choice == '!':
            print('Launching !')
            animation, trace_data_x, trace_data_y, time, delta_time, _, _ = scene.play()
        elif choice == 'l':
            _ = scene.look()
        elif choice == 'p':
            power = input(
                f'Current power: {int(playable_body.rigid_body.get_power())}\n'
                f'Set new power (0-100): '
            )

            try:
                power = int(power)
            except ValueError:
                print(f"{power} is not a number, power set to 0")
                power = 0

            angle = input(
                f'Current angle: {int(playable_body.rigid_body.get_angle())}°\n'
                f'Set new angle (°): '
            )

            try:
                angle = int(angle)
            except ValueError:
                print(f"{angle} is not a number, angle set to 0°")
                angle = 0

            playable_body.rigid_body.set_velocity_from_power(power, angle)

            if isinstance(playable_body, game_objects.Missile):
                playable_body.update_rocket_force()
        else:
            print(f"You typed '{choice}', this is not available")

        if choice == '!' and not win_edge.rigid_body.trigger:
            print(f"You missed, try again !")
            # Reset level
            scene = copy.deepcopy(scenes[scene_id - 1])
            world, playable_body, win_edge, frames, level_title, activate_trace = scene.load()

    # Win condition
    if win_edge.rigid_body.trigger:
        if scene_id >= len(scenes):
            print(f"\nCongratulations ! You finished the game !\n")
            return False, animation, trace_data_x, trace_data_y, time, delta_time
        else:
            scene_manager(scene_id + 1, scenes)


def main_menu(start_level=1, scenes: list[GameScene] = None):
    """
    Main menu of the game.
    """
    # Initialization
    if scenes is None:
        scenes = load_default_scenes()

    exit_game = False

    # Main loop
    while not exit_game:
        print(f'Main menu')

        # Wait for player input
        choice = input('What do you want to do ?\ns: start game\nln: load level n\nquit: quit\n')

        # Execute player action
        if choice == 's':
            outputs = scene_manager(start_level, scenes)
            exit_game = outputs
            continue
        elif 'l' in choice:
            start_level = choice.rsplit('l', 1)[-1]  # get the level id

            # Check if the level id is valid
            try:
                start_level = int(start_level)  # convert level id into an integer
            except ValueError:
                print(f"Loaded level must be a number, not '{choice.rsplit('l', 1)[-1]}'")
                continue

            outputs = scene_manager(start_level, scenes)
            exit_game = outputs
            continue
        elif choice == 'quit':
            break
        else:
            print(f"You typed '{choice}', this is not valid")
            continue

    print('Exiting game...')
