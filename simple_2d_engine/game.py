"""
Main script
"""
from simple_2d_engine import game_objects
from simple_2d_engine import scenes


def init_game():
    """Easy access to playable objects parameters."""
    # Set the apple parameters
    game_objects.Apple.default_radius = 6e-2  # (m) radius of the apple
    game_objects.Apple.default_mass = 100e-3  # (kg) mass of the apple
    game_objects.Apple.default_bounce_coefficient = 0.05  # bounce coefficient of the apple
    game_objects.Apple.default_drag_coefficient = 0.47  # drag coefficient of the apple
    game_objects.Apple.default_color = 'r'  # color of the apple
    game_objects.Apple.default_linewidth = 3  # line width of the apple

    # Set the hockey puck parameters
    game_objects.HockeyPuck.default_radius = 76e-3 / 2  # (m) radius of the hockey puck
    game_objects.HockeyPuck.default_mass = 170e-3  # (kg) mass of the hockey puck
    game_objects.HockeyPuck.default_bounce_coefficient = 0.9  # bounce coefficient of the hockey puck
    game_objects.HockeyPuck.default_drag_coefficient = 1.17  # drag coefficient of the hockey puck
    game_objects.HockeyPuck.default_color = 'r'  # color of the hockey puck
    game_objects.HockeyPuck.default_linewidth = 3  # line width of the hockey puck

    # Set the missile parameters
    game_objects.Missile.default_radius = 8e-2  # (m) radius of the missile
    game_objects.Missile.default_mass = 29  # (kg) mass of the missile
    game_objects.Missile.default_bounce_coefficient = 0.1  # bounce coefficient of the missile
    game_objects.Missile.default_drag_coefficient = 0.295  # drag coefficient of the missile
    game_objects.Missile.default_color = 'r'  # color of the missile
    game_objects.Missile.default_linewidth = 3  # line width of the missile
    game_objects.Missile.default_thrust = 3000.0  # (N) thrust of the missile
    game_objects.Missile.default_fuel_time = 2.0  # (s) thrust duration of the missile

    # Set the parachutist parameters
    game_objects.Parachutist.default_radius = 0.15  # (m) radius of the parachutist
    game_objects.Parachutist.default_mass = 70  # (kg) mass of the parachutist
    game_objects.Parachutist.default_bounce_coefficient = 0.1  # bounce coefficient of the parachutist
    game_objects.Parachutist.default_drag_coefficient = 1.00  # drag coefficient of the parachutist
    game_objects.Parachutist.default_parachute_radius = 2.351  # (m) radius of the parachute
    game_objects.Parachutist.default_parachute_open_time = 3.0  # (s) time before the parachute opening
    game_objects.Parachutist.default_parachute_drag_coefficient = 1.2  # drag coefficient of the parachute
    game_objects.Parachutist.default_color = 'r'  # color of the parachutist
    game_objects.Parachutist.default_linewidth = 3  # line width of the parachutist

    # Set the shell parameters
    game_objects.Shell.default_radius = 6e-2  # (m) radius of the shell
    game_objects.Shell.default_mass = 14.3  # (kg) mass of the shell
    game_objects.Shell.default_bounce_coefficient = 0.1  # bounce coefficient of the shell
    game_objects.Shell.default_drag_coefficient = 0.295  # drag coefficient of the shell
    game_objects.Shell.default_color = 'r'  # color of the shell
    game_objects.Shell.default_linewidth = 3  # line width of the shell


def main():
    init_game()

    scenes.main_menu()


if __name__ == '__main__':
    main()
