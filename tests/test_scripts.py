import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Circle, Rectangle

from simple_2d_engine import game_objects
from simple_2d_engine.animation import play_animation
from simple_2d_engine.colliders import Sphere, Edge
from simple_2d_engine.scenes import Scene
from simple_2d_engine.physics import Force, RigidBody, Vector2D
from simple_2d_engine.world import World


def test_box_box_collision():
    """Test collisions between boxes."""
    world = World(
        boundaries=np.array([[0, 0], [2, 2]]),
        gravity=0,
        bounce_enabled=True
    )

    playable_object = game_objects.ObstacleBox(
        size=(0.1, 0.25),
        position=(0.1, 1),
        velocity=(0.5, 0.5),
        world=world,
        color='r',
        drag_coefficient=1.0,
        bounce_coefficient=0.9,
        bounce_enabled=True,
        use_world_gravity=True,
        use_world_drag=True
    )

    box = game_objects.ObstacleBox(
        size=(1.0, 0.8, 1.0),
        position=(0.5, 0.6),
        bounce_coefficient=1,
        world=world,
    )

    world.objects = np.append(world.objects, [playable_object, box])

    animation, trace_data_x, trace_data_y, time, delta_time, fig, ax = play_animation(
        world, n_frames=1000, time_interval=0.03, speed=1, x_lim=world.boundaries[:, 0], y_lim=world.boundaries[:, 1],
        save_to_js_html=False, trace=False
    )

    # Ensure axes are plotted correctly
    ax.set_aspect(1)
    ax.set_xlim(world.boundaries[:, 0])
    ax.set_ylim(world.boundaries[:, 1])
    ax.set_navigate(False)
    plt.show()

    return animation, trace_data_x, trace_data_y, time, delta_time, fig, ax


def test_drag(ball_radius=0.75, ball_mass=15, ball_velocity=(100, 100), ball_drag_coefficient=0.47,
              world_x_lim=(0, 100), world_y_lim=(0, 100), forces=None,
              frames=300, save_to_js_html=False):
    """Test the drag force."""
    scene = Scene(
        title='Drag test',
        n_frames=frames,
        world=World(
            boundaries=np.array([[world_x_lim[0], world_y_lim[0]], [world_x_lim[1], world_y_lim[1]]]),
            gravity=9.81,
            bounce_enabled=False
        ),
        objects=[
            game_objects.GameObject.new_rigid_sphere(
                radius=ball_radius,
                position=(1, 1),
                velocity=ball_velocity,
                mass=ball_mass,
                bounce_coefficient=0.5,
                forces=forces,
                drag_coefficient=ball_drag_coefficient,  # 0.47 sphere, 0.295 bullet
                bounce_enabled=True,
                use_world_drag=True,
                use_world_gravity=True
            ),
            game_objects.WinEdge(
                length=0.5,
                rotation=90,
                position=(1.9, 0.75)
            )
        ]
    )

    scene.load()

    animation, trace_data_x, trace_data_y, time, delta_time, fig, ax = scene.setup_play(
        save_to_js_html=save_to_js_html
    )
    scene.pyplot_show(ax)

    return animation, trace_data_x, trace_data_y, time, delta_time


def test_marble_run():
    """Test different collisions between a sphere and edges."""
    # TODO when the sphere collides an edge on the side, the sphere shouldn't go "into" the edge
    world = World(boundaries=np.array([[0, 0], [15, 100]]), gravity=9.81)

    collider = Sphere(1.0)
    rigid_body = RigidBody(
        position=(10, 95),
        velocity=(0, 0),
        collider=collider,
        mass=15,
        bounce_coefficient=0.1,
        drag_coefficient=0.47,  # 0.47 sphere, 0.295 bullet
        world=world,
        forces=[Force(Vector2D(0.0, 0.0))],
        bounce_enabled=True,
        use_world_drag=True,
        use_world_gravity=True
    )
    sprite = Circle(
        xy=(rigid_body.position.x, rigid_body.position.y),
        radius=rigid_body.collider.radius,
        color='r',
        fill=False
    )

    static_edge1 = RigidBody(
        position=(3, 90),
        velocity=(0, 0),
        collider=Edge(15, 20),
        mass=1,
        bounce_coefficient=0,
        drag_coefficient=0,  # 0.47 sphere, 0.295 bullet
        world=world,
        bounce_enabled=True,
        use_world_drag=False,
        use_world_gravity=False
    )
    static_edge2 = RigidBody(
        position=(0, 80),
        velocity=(0, 0),
        collider=Edge(10, -5),
        mass=1,
        bounce_coefficient=0,
        drag_coefficient=0,  # 0.47 sphere, 0.295 bullet
        world=world,
        bounce_enabled=True,
        use_world_drag=False,
        use_world_gravity=False
    )
    static_edge3 = RigidBody(
        position=(5, 60),
        velocity=(0, 0),
        collider=Edge(15, 45),
        mass=1,
        bounce_coefficient=0,
        drag_coefficient=0,  # 0.47 sphere, 0.295 bullet
        world=world,
        bounce_enabled=True,
        use_world_drag=False,
        use_world_gravity=False
    )
    static_edge4 = RigidBody(
        position=(0, 50),
        velocity=(0, 0),
        collider=Edge(6.2, -10),
        mass=1,
        bounce_coefficient=0,
        drag_coefficient=0,  # 0.47 sphere, 0.295 bullet
        world=world,
        bounce_enabled=True,
        use_world_drag=False,
        use_world_gravity=False
    )
    static_edge5 = RigidBody(
        position=(15, 50),
        velocity=(0, 0),
        collider=Edge(6.2, 190),
        mass=1,
        bounce_coefficient=0,
        drag_coefficient=0,  # 0.47 sphere, 0.295 bullet
        world=world,
        bounce_enabled=True,
        use_world_drag=False,
        use_world_gravity=False
    )
    static_edge6 = RigidBody(
        position=(5, 35),
        velocity=(0, 0),
        collider=Edge(5, 45),
        mass=1,
        bounce_coefficient=0,
        drag_coefficient=0,  # 0.47 sphere, 0.295 bullet
        world=world,
        bounce_enabled=True,
        use_world_drag=False,
        use_world_gravity=False
    )

    sprite1 = Rectangle(
        xy=(static_edge1.position.x, static_edge1.position.y),
        width=static_edge1.collider.length,
        height=0,
        angle=static_edge1.collider.rotation.z,
        color='k',
        fill=True
    )
    sprite2 = Rectangle(
        xy=(static_edge2.position.x, static_edge2.position.y),
        width=static_edge2.collider.length,
        height=0,
        angle=static_edge2.collider.rotation.z,
        color='k',
        fill=True
    )
    sprite3 = Rectangle(
        xy=(static_edge3.position.x, static_edge3.position.y),
        width=static_edge3.collider.length,
        height=0,
        angle=static_edge3.collider.rotation.z,
        color='k',
        fill=True
    )
    sprite4 = Rectangle(
        xy=(static_edge4.position.x, static_edge4.position.y),
        width=static_edge4.collider.length,
        height=0,
        angle=static_edge4.collider.rotation.z,
        color='k',
        fill=True
    )
    sprite5 = Rectangle(
        xy=(static_edge5.position.x, static_edge5.position.y),
        width=static_edge5.collider.length,
        height=0,
        angle=static_edge5.collider.rotation.z,
        color='k',
        fill=True
    )
    sprite6 = Rectangle(
        xy=(static_edge6.position.x, static_edge6.position.y),
        width=static_edge6.collider.length,
        height=0,
        angle=static_edge6.collider.rotation.z,
        color='k',
        fill=True
    )

    ball = game_objects.GameObject(rigid_body, sprite)
    edge_static1 = game_objects.GameObject(static_edge1, sprite1)
    edge_static2 = game_objects.GameObject(static_edge2, sprite2)
    edge_static3 = game_objects.GameObject(static_edge3, sprite3)
    edge_static4 = game_objects.GameObject(static_edge4, sprite4)
    edge_static5 = game_objects.GameObject(static_edge5, sprite5)
    edge_static6 = game_objects.GameObject(static_edge6, sprite6)

    world.objects = np.append(
        world.objects, [ball, edge_static1, edge_static2, edge_static3, edge_static4, edge_static5, edge_static6]
    )

    animation, trace_data_x, trace_data_y, time, delta_time, fig, ax = play_animation(
        world, n_frames=3000, time_interval=0.03, speed=1, y_lim=(0, 100), x_lim=(0, 15),
        save_to_js_html=False, trace=True
    )

    # Ensure axes are plotted correctly
    ax.set_aspect(1)
    ax.set_xlim(world.boundaries[:, 0])
    ax.set_ylim(world.boundaries[:, 1])
    ax.set_navigate(False)
    plt.show()

    return animation, trace_data_x, trace_data_y, time, delta_time


def test_stickiness():
    """Test if spheres correctly "roll" on edges and doesn't "stick" to them."""
    collider = Sphere(1.0)
    rigid_body = RigidBody(
        position=(10, 13),
        velocity=(0, -5),
        collider=collider,
        mass=15,
        bounce_coefficient=0.1,
        drag_coefficient=0.47,  # 0.47 sphere, 0.295 bullet
        forces=[Force(Vector2D(0.0, 0.0))],
        bounce_enabled=True,
        use_world_drag=True,
        use_world_gravity=True
    )
    sprite = Circle(
        xy=(rigid_body.position.x, rigid_body.position.y),
        radius=rigid_body.collider.radius,
        color='k',
        fill=False
    )

    collider3 = Edge(15, 20)
    static_edge = RigidBody(
        position=(5, 6),
        velocity=(0, 0),
        collider=collider3,
        mass=1,
        bounce_coefficient=0,
        drag_coefficient=0,  # 0.47 sphere, 0.295 bullet
        bounce_enabled=True,
        use_world_drag=False,
        use_world_gravity=False
    )
    sprite2 = Rectangle(
        xy=(static_edge.position.x, static_edge.position.y),
        width=static_edge.collider.length,
        height=0,
        angle=static_edge.collider.rotation.z,
        color='b',
        fill=True
    )

    ball = game_objects.GameObject(rigid_body, sprite)
    edge_static = game_objects.GameObject(static_edge, sprite2)

    scene = Scene(
        title='Stickiness test',
        n_frames=300,
        activate_trace=True,
        world=World(
            boundaries=np.array([[0, 0], [15, 15]]),
            gravity=9.81
        ),
        objects=[
            ball,
            edge_static
        ]
    )

    scene.load()

    animation, trace_data_x, trace_data_y, time, delta_time, fig, ax = scene.play()

    return animation, trace_data_x, trace_data_y, time, delta_time, fig, ax


def test_strafrunning(player_velocity):
    """Test if velocity is correctly calculated."""
    scene = Scene(
        title='Strafrunning test',
        n_frames=300,
        world=World(
            boundaries=np.array([[0, 0], [2, 2]]),
            gravity=0,
            bounce_enabled=False
        ),
        objects=[
            game_objects.HockeyPuck(
                position=(1, 1),
                velocity=player_velocity
            ),
            game_objects.HockeyPuck(
                position=(1, 1),
                mass=1e-30,
                radius=0.6,
                color='gold',
                bounce_enabled=False
            )
        ]
    )

    scene.load()

    animation, trace_data_x, trace_data_y, time, delta_time, fig, ax = scene.setup_play(
        time_interval=0.03,
        speed=0.03
    )
    scene.pyplot_show(ax)

    return animation, trace_data_x, trace_data_y, time, delta_time, fig, ax


def test_sphere_box_box_collision():
    """Test collisions between a sphere and 2 boxes."""
    # TODO collisions between spheres and boxes
    world = World(
        boundaries=np.array([[0, 0], [2, 2]]),
        gravity=0,
        bounce_enabled=True
    )

    playable_object = game_objects.HockeyPuck(
        position=(0.25, 1),
        velocity=(0.5, 0.5),
        world=world,
    )

    box = game_objects.ObstacleBox(
        size=(1.25, 1.25, 1.25),
        position=(0.375, 0.375),
        velocity=(0.0, 0.0),
        bounce_coefficient=0.9,
        world=world,
    )

    box2 = game_objects.ObstacleBox(
        size=(0.1, 0.1, 0.1),
        position=(1.85, 1),
        velocity=(0.5, 0.5),
        bounce_coefficient=0.9,
        color='b',
        world=world,
    )

    # world.objects = np.append(world.objects, [playable_object, box])
    world.objects = np.append(world.objects, [playable_object, box, box2])

    animation, trace_data_x, trace_data_y, time, delta_time, fig, ax = play_animation(
        world, n_frames=1000, time_interval=0.03, speed=1, x_lim=world.boundaries[:, 0], y_lim=world.boundaries[:, 1],
        save_to_js_html=False, trace=False
    )

    # Ensure axes are plotted correctly
    ax.set_aspect(1)
    ax.set_xlim(world.boundaries[:, 0])
    ax.set_ylim(world.boundaries[:, 1])
    ax.set_navigate(False)
    plt.show()

    return animation, trace_data_x, trace_data_y, time, delta_time, fig, ax


def test_sphere_sphere_edge_collision():
    """Test collisions between multiple spheres and edges."""
    world = World(boundaries=np.array([[0, 0], [15, 15]]), gravity=9.81)

    collider = Sphere(1.0)
    rigid_body = RigidBody(
        position=(7, 1),
        velocity=(10, 30),
        collider=collider,
        mass=15,
        bounce_coefficient=0.5,
        drag_coefficient=0.47,  # 0.47 sphere, 0.295 bullet
        world=world,
        forces=[Force(Vector2D(0.0, 0.0))],
        bounce_enabled=True,
        use_world_drag=True,
        use_world_gravity=True
    )
    sprite = Circle(
        xy=(rigid_body.position.x, rigid_body.position.y),
        radius=rigid_body.collider.radius,
        color='k',
        fill=False
    )

    collider2 = Sphere(1.2)
    rigid_body2 = RigidBody(
        position=(9, 10),
        velocity=(0, -5),
        collider=collider2,
        mass=150,
        bounce_coefficient=0.6,
        drag_coefficient=0.47,  # 0.47 sphere, 0.295 bullet
        world=world,
        bounce_enabled=True,
        use_world_drag=True,
        use_world_gravity=True
    )
    sprite2 = Circle(
        xy=(rigid_body2.position.x, rigid_body2.position.y),
        radius=rigid_body2.collider.radius,
        color='r',
        fill=False
    )

    collider3 = Edge(10, 20)
    static_edge = RigidBody(
        position=(5, 6),
        velocity=(0, 0),
        collider=collider3,
        mass=1,
        bounce_coefficient=0,
        drag_coefficient=0,  # 0.47 sphere, 0.295 bullet
        world=world,
        bounce_enabled=True,
        use_world_drag=False,
        use_world_gravity=False
    )
    sprite3 = Rectangle(
        xy=(static_edge.position.x, static_edge.position.y),
        width=static_edge.collider.length,
        height=0,
        angle=static_edge.collider.rotation.z,
        color='b',
        fill=True
    )

    collider32 = Edge(10, -100)
    static_edge2 = RigidBody(
        position=static_edge.collider.get_vertices(static_edge)[1],
        velocity=(0, 0),
        collider=collider32,
        mass=1,
        bounce_coefficient=0,
        drag_coefficient=0,  # 0.47 sphere, 0.295 bullet
        world=world,
        bounce_enabled=True,
        use_world_drag=False,
        use_world_gravity=False
    )
    sprite32 = Rectangle(
        xy=(static_edge2.position.x, static_edge2.position.y),
        width=static_edge2.collider.length,
        height=0,
        angle=static_edge2.collider.rotation.z,
        color='b',
        fill=True
    )

    ball = game_objects.GameObject(rigid_body, sprite)
    ball_static = game_objects.GameObject(rigid_body2, sprite2)
    edge_static = game_objects.GameObject(static_edge, sprite3)
    edge_static2 = game_objects.GameObject(static_edge2, sprite32)

    world.objects = np.append(world.objects, [ball, ball_static, edge_static, edge_static2])

    animation, trace_data_x, trace_data_y, time, delta_time, fig, ax = play_animation(
        world, n_frames=300, time_interval=0.03, speed=1, y_lim=(0, 15), x_lim=(0, 15),
        save_to_js_html=False, animation_file='./two_spheres_two_edges.html', trace=True
    )

    # Ensure axes are plotted correctly
    ax.set_aspect(1)
    ax.set_xlim(world.boundaries[:, 0])
    ax.set_ylim(world.boundaries[:, 1])
    ax.set_navigate(False)
    plt.show()

    return animation, trace_data_x, trace_data_y, time, delta_time
