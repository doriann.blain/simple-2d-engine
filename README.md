# Simple 2D engine
## Synopsis


This package was initially intended to teach physics applied to video games. It is a simple 2D engine with in-script editable levels and basic player inputs. The player controls a red circle that can represents different objects depending on the level (e.g., a hockey puck, a parachutist or a rocket). The player can only "shoot" the red circle, changing the angle and "power" of the shot. In each level, the goal is for the red circle to touch a golden "win edge".


## Motivation
These modules were intended for educational purposes, in order to teach the basics of physics applied to video games. Hence, only the `numpy` (for calculations), `matplotlib` and `PyQt5` (for display) libraries are used in order for students to understand how a physic engine might work down to a low abstraction level. This is the package version, not the version used for teaching.

## Installation
1. Download or clone this repository anywhere you want. 
2. In the downloaded directory, open a terminal and execute: 
   ```bash
   pip install .
   ```

## Usage
### Default game
Launch the game by executing:
   ```bash
   simple_2d_engine_game
   ```
in a terminal.

Alternatively: 
1. Open a terminal.
2. `cd` yourself into the rpg directory by executing:
    ```bash
    cd path/to/simple_2d_engine_game/simple_2d_engine_game
    ```
    where `path/to/` must be replaced by the path where you installed/cloned this repository.
3. Execute:
    ```bash
    python ./game.py
    ```

This will open the main menu:
   ```
   Main menu
   What do you want to do ?
   s: start game
   ln: load level n
   quit: quit
   ```

Pressing `s` then "Enter" will open a window displaying level 1:

<div align="center">
<img src="./images/level1.png" alt="Level 1" width="220"/>
</div>

And print the command menu:
   ```
   Level 1: First strike!
   
   Power: 0
   Angle: 0°
   
   What do you want to do ?
   !: launch !
   l: look level
   p: set power and angle
   r: return to menu
   quit: quit
   ```

The game has 7 levels of increasing difficulty. Have fun!

### Custom scenes
It is possible to run custom scenes by using the `Scene` object. For example:
```python
import numpy as np
import simple_2d_engine.game as e2d

scene = e2d.scenes.Scene(
   activate_trace=True,  # if True, objects will leave a trace behind them
   n_frames=300,  # number of frames to play the scene
   world=e2d.scenes.World(
      boundaries=np.array([[0, 0], [100, 20]]),  # x boundaries: [0, 100], y boundaries [0, 20] 
      gravity=9.81,  # gravity of the world in units.s-2
      fluid_density=1.2,  # density of air in kg.units-3, for a bit of drag
      bounce_enabled=True  # if True, objects will bounce on the world boundaries
   ),
   objects=[
      e2d.game_objects.HockeyPuck(
         position=(1, 1),  # x and y start position within the world (must be within the world x and y boundaries)
         velocity=(20, 20)  # velocity vector (x and y axes)
      ),
      e2d.game_objects.ObstacleEdge(
         length=2,  # length of the edge, in world units
         rotation=90,  # rotation of the edge, in degrees (horizontal at 0°)
         position=(50, 7)  # x and y position of the edge
      ),
   ]
)

scene.load()
_ = scene.play()
```
<div align="center">
<img src="./images/example1.gif" alt="Custom scene animation" width="440"/>
</div>

### Custom games
It is also possible to make custom games by initializing a list of `GameScene` objects, for example:
```python
import numpy as np
import simple_2d_engine.game as e2d

# Initialize the scene(s)
scenes = [
   e2d.scenes.GameScene(  # level 1
      title='My level 1',  # title to be displayed in the menu
      activate_trace=False,  # if True, objects will leave a trace behind them
      n_frames=150,  # number of frames to play the scene
      world=e2d.scenes.World(
         boundaries=np.array([[0, 0], [15, 15]]),
         gravity=3.33,
         bounce_enabled=False
      ),
      playable_object=e2d.game_objects.HockeyPuck(
         position=(7, 1)
      ),
      win_edge=e2d.game_objects.WinEdge(
         length=0.5,
         rotation=90,
         position=(1.9, 0.75)
      ),
      objects=[
         e2d.game_objects.GameObject.new_rigid_sphere(  # another sphere looming above
            radius=1.2,
            position=(9, 10),
            velocity=(0, -5),
            mass=150,
            bounce_coefficient=0.6,
            bounce_enabled=True,
            use_world_gravity=True,
            use_world_drag=True
         ),
         e2d.game_objects.ObstacleEdge(  # an edge protecting from the other sphere
            length=10,
            rotation=20,
            position=(5, 6),
            bounce_coefficient=0,
            color='k',
            linewidth=1
         ),
         e2d.game_objects.ObstacleEdge(  # the ground
            length=15,
            rotation=0,
            position=(0, 0.1),
            bounce_coefficient=0,
            color='g',
            linewidth=3
         )
      ]
   ),
   e2d.scenes.GameScene(  # level 2
      ...
   ),
   ...  # more levels
]

# Start the game
e2d.scenes.main_menu(start_level=1, scenes=scenes)
```
Below is how "My level 1" will play after setting the power and angle of the playable object (here, the red circle).

<div align="center">
<img src="./images/example2.gif" alt="Level 1 of custom game" width="440"/>
</div>
